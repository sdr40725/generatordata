﻿using System;

namespace GeneratorData
{
    public static class Helper
    {
        public static double AwayFromZero(this double d)
        {
            return Math.Round(d, 2, MidpointRounding.AwayFromZero);
        }
    }
}
