﻿using System;

namespace GeneratorData
{
    class Program
    {
        static void Main(string[] args)
        {
            var randomGenerateRoster = new RandomGenerateRoster(100000,1,100,70);
            var randomRoster = randomGenerateRoster.GenerateData();
            randomRoster.AmountList.ForEach(Console.WriteLine);

            Console.WriteLine("END...");
            Console.Read();
        }
    }
}
