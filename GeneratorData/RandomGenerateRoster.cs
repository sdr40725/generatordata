﻿using System;
using System.Collections.Generic;
using System.Linq;
using GeneratorData.Enum;

namespace GeneratorData
{
    public class RandomGenerateRoster
    {
        private readonly double _avg;
        private readonly int _count;
        private readonly double _expected;
        private readonly double _max;
        private readonly double _min;
        private readonly Random _random = new Random();
        private readonly ClosestType _closestType;
        private double _diffValue;
        private List<double> _numbers;
        private double _sum;

        public RandomGenerateRoster(int count,
                                    double min,
                                    double max,
                                    double avg)
        {
            _count = count;
            _min = min;
            _max = max;
            _avg = avg;
            _expected = _count * _avg;
            _numbers = InitNumbers();
            _sum = _numbers.Sum();
            _closestType = GetClosestType();
        }

        public RandomRoster GenerateData()
        {
            ReCalculateClosestAvg();
            ReSortRandomNumbers();

            return new RandomRoster
            {
                AmountTotal = _numbers.Sum().AwayFromZero(),
                AmountAvg = _numbers.Average().AwayFromZero(),
                AmountList = _numbers
            };
        }

        private void ReCalculateClosestAvg()
        {
            var count = 0;

            if (GetCurrentAvg().Equals(_avg) == false)
            {
                for (var index = 0; index < _count; index++)
                {
                    if (CheckReCalculateValue(index))
                    {
                        var oldValue = _numbers[index];
                        _numbers[index] = _closestType == ClosestType.BaseOldValueAddRandom ?
                                                          GetBaseOldValueAddRandom(oldValue).AwayFromZero():
                                                          GetBaseOldValueSubRandom(oldValue).AwayFromZero();
                        _sum += _numbers[index] - oldValue;
                        count += 1;
                    }
                }

                _diffValue = _expected - _sum;
                if (Math.Abs(_diffValue) > 0) FillDiffValue();
            }

            Console.WriteLine(count);
        }

        private bool CheckReCalculateValue(int index)
        {
            return IsBaseOldValueAddRandom(index) || IsBaseOldValueSubRandom(index);
        }

        private ClosestType GetClosestType()
        {
            return GetCurrentAvg() < _avg ? ClosestType.BaseOldValueAddRandom :
                   GetCurrentAvg() > _avg ? ClosestType.BaseOldValueSubRandom : 
                   ClosestType.None;
        }

        private bool IsBaseOldValueAddRandom(int i)
        {
            return _closestType == ClosestType.BaseOldValueAddRandom &&
                   GetCurrentAvg() < _avg &&
                   _numbers[i] < _avg;
        }

        private bool IsBaseOldValueSubRandom(int i)
        {
            return _closestType == ClosestType.BaseOldValueSubRandom &&
                   GetCurrentAvg() > _avg &&
                   _numbers[i] > _avg;
        }

        private void FillDiffValue()
        {
            //var oldValue = _closestType == ClosestType.BaseOldValueAddRandom ?
            //                     _numbers.First(number => number + _diffValue <= _max):
            //                     _numbers.First(number => number + _diffValue >= _min);

            var oldValue = _diffValue > 0 ?
                _numbers.First(number => number + _diffValue <= _max) :
                _numbers.First(number => number + _diffValue >= _min);

            var newValue = oldValue + _diffValue;

            if (newValue <= 0)
            {
                var bb = "";
            }
            else if(newValue < _min)
            {
                var aa = "";
            }
            else if (newValue > _max)
            {
                var aa = "";
            }

            ReplaceNumber(oldValue, newValue);
        }

        private double GetCurrentAvg()
        {
            return _sum / _count;
        }

        private double GetBaseOldValueSubRandom(double oldValue)
        {
            var aa = RangedDouble(_min, _avg);
            return aa;
        }

        private double GetBaseOldValueAddRandom(double oldValue)
        {
            var bb = RangedDouble(_avg, _max);
            return bb;
        }

        private void ReSortRandomNumbers()
        {
            _numbers = _numbers.OrderBy(number => Guid.NewGuid()).ToList();
        }

        private List<double> InitNumbers()
        {
            _numbers = new List<double>(_count);
            for (var i = 0; i < _count; i++) _numbers.Add(RangedDouble(_min, _max));
            return _numbers;
        }

        private void ReplaceNumber(double oldValue, double newValue)
        {
            _numbers.Remove(oldValue);
            _sum -= oldValue;
            _numbers.Add(Math.Round(newValue, 2, MidpointRounding.AwayFromZero));
            _sum += Math.Round(newValue, 2, MidpointRounding.AwayFromZero);
        }

        private double RangedDouble(double min, double max)
        {
            var result = Math.Round(_random.NextDouble() * (max - min) + min, 2, MidpointRounding.AwayFromZero);
            return result;
        }
    }
}