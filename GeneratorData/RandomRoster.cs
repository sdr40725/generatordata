﻿using System.Collections.Generic;

namespace GeneratorData
{
    public class RandomRoster
    {
        public double AmountAvg { get; set; }
        public double AmountTotal { get; set; }
        public List<double> AmountList { get; set; }
    }
}