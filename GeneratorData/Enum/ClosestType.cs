﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GeneratorData.Enum
{
    public enum ClosestType
    {
        None = 0,
        BaseOldValueAddRandom = 1,
        BaseOldValueSubRandom = 2,
    }
}
