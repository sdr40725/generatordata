﻿using System.Diagnostics;
using System.Linq;
using NUnit.Framework;

namespace GeneratorData.test
{
    [TestFixture]
    public class RandomGenerateData_test
    {
        [Test]
        public void GenerateData()
        {
            var randomGenerateData = new RandomGenerateRoster(10, 1, 100, 50);
            var generator = randomGenerateData.GenerateData();
            
            Assert.AreEqual(50m,generator.AmountAvg);
            Assert.AreEqual(10,generator.AmountList.Count);
            Assert.AreEqual(500m,generator.AmountTotal);
            Assert.False(generator.AmountList.All(s=> s == 50));
            Assert.False(generator.AmountList.Any(s => s <= 0));
        }

        //[TestCase(10, 1, 100, 50)]
        //[TestCase(100, 1, 100, 70)]
        //[TestCase(7000, 20, 40, 35)]
        //[TestCase(8500, 1, 25, 5)]
        //[TestCase(10, 70, 90, 75)]
        //[TestCase(10, 70, 90, 75)]
        //[TestCase(10, 70, 90, 75)]
        //[TestCase(10, 70, 90, 75)]
        //[TestCase(10, 1, 10, 9)]
        //[TestCase(10, 90, 100, 99)]
        //[TestCase(10, 90, 100, 91)]
        //[TestCase(10, 1, 100, 50)]
        //[TestCase(10, 1, 100, 50)]
        //[TestCase(10, 1, 100, 50)]
        //[TestCase(10, 1, 100, 50)]
        //[TestCase(10, 1, 100, 50)]
        //[TestCase(10, 1, 100, 50)]
        //[TestCase(10, 1, 100, 50)]
        //[TestCase(10, 1, 100, 50)]
        [TestCase(10, 90, 100, 91)]
        public void GenerateData_Avg(int count, double min, double max, double avg)
        {
            var randomGenerateData = new RandomGenerateRoster(count, min, max, avg);
            var generator = randomGenerateData.GenerateData();

            Assert.AreEqual(avg, generator.AmountAvg);
            Assert.AreEqual(count, generator.AmountList.Count);
            Assert.AreEqual(avg * count, generator.AmountTotal);
            Assert.False(generator.AmountList.All(s => s == avg));
            Assert.False(generator.AmountList.Any(amount => amount < 0));
            Assert.False(generator.AmountList.Any(amount => amount > (double)max));
            Assert.False(generator.AmountList.Any(amount => amount < (double)min));
        }


    }
}